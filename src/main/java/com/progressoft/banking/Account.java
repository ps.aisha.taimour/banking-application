package com.progressoft.banking;

public interface Account {

    int getBalance();

    void deposit(int amount);

    void withdraw(int amount);

    void getPreviousTransaction();

}
