
##Simple Banking Application

In this banking application you will be coding the simple bank operations like check balance, deposit, withdraw, exit, etc.

When you run the application, It will show you a list of the available operations as the following:

    Welcome "Customer Name", your account number is "Customer Account Number".
    
    Choose an option please:
        A. Check Balance
        B. Deposit
        C. Withdraw
        D. Previous Transaction
        E. Exit
        
Initially the balance will be zero, and the user can't withdraw money, until he deposit money in his account. 
and the previous transaction will show him the last deposit or withdraw operation he did, if there is no transaction yet, 
the system will show him a message 'No transaction occurred'.

Hint: You will take input from the user using Scanner class.